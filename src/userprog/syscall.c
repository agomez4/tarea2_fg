#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "threads/synch.h"
#include "filesys/filesys.h"

static void syscall_handler(struct intr_frame *);
static void syscall_halt(void);
static void syscall_exit( struct intr_frame *);
static int syscall_wait(tid_t tid, struct intr_frame *);
static tid_t syscall_exec(const char *, struct intr_frame *);

// Implementation Parameters
// =========================
static int WRITE_SIZE=128;
static struct lock filesys_lock;



inline
int
syscall_write(struct intr_frame *f)
{
  int *p = f->esp;  // Stack pointer (x86 registers http://www.scs.stanford.edu/05au-cs240c/lab/i386/s02_03.htm)

  //FIXME: This is (probably) displaced because of the -12 on process.c:442 (setup_stack)
  //int file_descriptor =         p[1];
  //char        *buffer = (char*) p[2];
  //int            size = (int)   p[3];  // huge size may cause overflow

  int file_descriptor =         p[5];
  char        *buffer = (char*) p[6];
  int            size = (int)   p[7];  // may overflow signed int!!

  switch(file_descriptor)
  {
    case STDIN_FILENO:
      // Inform that no data was written
      f->eax=0;
      // REVIEW: Should that process be terminated?
      return 2;

    case STDOUT_FILENO:
    {
      // Write in chunks of WRITE_SIZE
      //   putbuf (src/lib/kernel/console.c) locks the console,
      //   we should avoid locking it too often or for too long
      int remaining = size;
      while(remaining > WRITE_SIZE)
      {
        // Write a chunk
        putbuf(buffer, WRITE_SIZE);
        // Advance buffer pointer
        buffer    += WRITE_SIZE;
        remaining -= WRITE_SIZE;
      }
      // Write all the remaining data
      putbuf(buffer, remaining);

      // Inform the amount of data written
      f->eax=(int)size;
      return 0;
    }

    default:
      printf("syscall: write call not implemented for files\n");
      return 1;
  }

  return 1;  // Unreachable, but compiler complains
}

static void
syscall_handler (struct intr_frame *f)
{
  // intr_frame holds CPU register data
  //   Intel 80386 Reference Programmer's Manual (TL;DR)
  //     http://www.scs.stanford.edu/05au-cs240c/lab/i386/toc.htm

  int *p = f->esp;  // Stack pointer (x86 registers http://www.scs.stanford.edu/05au-cs240c/lab/i386/s02_03.htm)
  int syscall_number = (*p);


  switch(syscall_number)
  {
    

    case SYS_HALT:
      printf("system call: halt\n");
      syscall_halt();
      break;


    case SYS_EXIT:
       
      printf("system call: exit\n");
      syscall_exit(f);
      return;
      break;

    case SYS_EXEC:
      printf("system call: exec\n");
      syscall_exec((char*)*(p +1),f);
      return;
      break;

    case SYS_WAIT:
      printf("system call: wait\n");
      syscall_wait((tid_t)*(p+1),f);
      return;
      break;

    case SYS_WRITE:
      syscall_write(f);
      return;

    default:
      printf("system call: unhandled syscall. Terminating process[%d]\n",
             thread_current()->tid);

      break;
  }

  // Syscall handling failed, termisnate the process
  syscall_exit(f);
}



void
syscall_init (void)
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");

  lock_init(&filesys_lock);




}

static void
syscall_halt(void)
{
  shutdown_power_off();
}


static void
syscall_exit(struct intr_frame *f)
{
  
  if(!thread_current()->no_success)
    thread_current()->exitCode = 0;
  sema_up(&thread_current()->semaforo_thread);
  f->eax = thread_current()->exitCode;
  sema_down(&thread_current()->semaforo_thread);
  printf("%d: exit(%d) \n", thread_current()-> tid, thread_current()->exitCode);
  thread_exit();
}


static int 
syscall_wait(tid_t tid, struct intr_frame *f)
{
  printf("Wait for %d\n", tid);
  int ret = process_wait(tid);
  f->eax = ret;
  return  ret;
}

static tid_t
syscall_exec(const char *file_name, struct intr_frame *f)
{
  lock_acquire(&filesys_lock);

  tid_t id= process_execute(file_name);


  lock_release(&filesys_lock);


  f -> eax = id;

  return id;
}